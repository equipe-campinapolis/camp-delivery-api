class Organization < ApplicationRecord
	has_many :products
	enum organization_type: [ :salesman,:deliveryman,:admin ], _suffix: true
	validates :username, uniqueness: true
end
